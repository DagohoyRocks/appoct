/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './src/screens/Auth/LoginScreen';
import ListScreen from './src/screens/Contents/ListScreen';
import DetailScreen from './src/screens/Contents/DetailScreen';

const App: () => React$Node = () => {
  const [isLogin, setLogin] = useState(false)

  useEffect(() => {
    checkToken()
  })
  //Check if login been made
  const checkToken = async () => {
    var token = await AsyncStorage.getItem('token')
    if (token != null && token != 'false') {
      setLogin(true)
    }
  }
  const StackAppScreen = createStackNavigator(); //Main stack route
  //Stack route for the screens after login
  const logined = () => {
    const Logined = createStackNavigator();
    return (
      <Logined.Navigator initialRouteName={"list"} screenOptions={{ headerShown: false }}>
        <Logined.Screen name="list" component={ListScreen} />
        <Logined.Screen name="detail" component={DetailScreen} />
      </Logined.Navigator>
    )
  }
  //Stack route for the screens for auth
  const Auth = () => {
    const Authenticate = createStackNavigator();
    return (
      <Authenticate.Navigator initialRouteName={"login"} screenOptions={{ headerShown: false }}>
        <Authenticate.Screen name="login" component={LoginScreen} />
      </Authenticate.Navigator>
    )
  }

  return (
    <NavigationContainer>
      <StackAppScreen.Navigator screenOptions={{ headerShown: false }}>
        {!isLogin ? <StackAppScreen.Screen name="Auth" component={Auth} /> : null}
        <StackAppScreen.Screen name="list" component={logined} />
      </StackAppScreen.Navigator>
    </NavigationContainer>
  );
};

export default App;

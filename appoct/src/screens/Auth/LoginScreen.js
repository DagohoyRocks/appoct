import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput
} from 'react-native';
import {
  containerStyle,
  textInputStyle,
  buttonStyle,
  textStyle
} from '../Styles/Styles';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions } from '@react-navigation/native';

export default function LoginScreen(props) {
  const { navigation, route } = props;
  const placeHolderEmailVal = 'Please Input Email'
  const placeHolderPass = 'Please Input Password'
  const [emailVal, onChangeEmailVal] = useState('')
  const [passwordVal, onChangePasswordVal] = useState('')

  useEffect(() => {

  })
  //The validation check only if email and password inputed
  var validate = async () => {

    if (emailVal.length != 0 && passwordVal.length != 0) {

      try {
        //store the mock up token to the storage
        await AsyncStorage.setItem('token', 'true')
        navigation.dispatch(StackActions.replace('list'))
      }
      catch (e) {
        console.log(e)
      }
    }
  }

  return (
    <View style={[containerStyle.loginView]}>
      <TextInput
        placeholder={placeHolderEmailVal}
        style={[textInputStyle.loginInputs]}
        onChangeText={text => onChangeEmailVal(text)}
        value={emailVal}
      />
      <TextInput
        placeholder={placeHolderPass}
        style={[textInputStyle.loginInputs]}
        onChangeText={text => onChangePasswordVal(text)}
        value={passwordVal}
      />
      <TouchableOpacity
        onPress={validate}
        style={[buttonStyle.loginBtn]}
      >
        <Text style={[textStyle.loginBtnTxt]}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  )
}


import React, { useState, useEffect } from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';


const RepoDetailsComponent = (props)=>(
  <View style={[props.containerStyle? props.containerStyle :{}]}>
    <Text style={[props.textLabelStyle? props.textLabelStyle :{}]}>Repo Name</Text>
    <Text style={[props.textStyle? props.textStyle :{}]}>{props.repoName?props.repoName : ''}</Text>
    <Text style={[props.textLabelStyle? props.textLabelStyle :{}]}>Total Of Stars</Text>
    <Text style={[props.textStyle? props.textStyle :{}]}>{props.stars?props.stars : ''}</Text>
    <TouchableOpacity
      onPress={props.onPress}
      style={[props.buttonStyle]}>
      <Text style={[props.textStyle? props.textStyle :{}]}>Go to repo</Text>
    </TouchableOpacity>
  </View>
)

export default RepoDetailsComponent;

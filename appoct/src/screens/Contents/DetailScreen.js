import React, { useState, useEffect } from 'react';
import {Linking} from 'react-native';
import RepoDetailsComponent from '../../../src/screens/Component/RepoDetailsComponent';
import {
  buttonStyle,
  containerStyle,
  textInputStyle,
  textStyle
} from '../Styles/Styles';


export default function DetailScreen(props) {
  const { navigation, route } = props;
  const details = route.params.data

  const onPress = () => {

    Linking.canOpenURL(details.html_url).then(canBeOpen => {
      if (canBeOpen) {
        Linking.openURL(details.html_url);
      } else {
        console.log("not supported URL: " + details.html_url);
      }
    });
  }
  return (
    <RepoDetailsComponent
      containerStyle={containerStyle.detailView}
      textLabelStyle={textStyle.label}
      textStyle={textStyle.input}
      buttonStyle={buttonStyle.loginBtn}
      repoName={details.name}
      stars={details.stargazers_count}
      onPress={() => onPress()}
    />
  )
}


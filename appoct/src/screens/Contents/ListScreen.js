import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  FlatList,
  ActivityIndicator
} from 'react-native';
import {
  containerStyle,
  textInputStyle,
  textStyle
} from '../Styles/Styles';

import {requestCall} from '../Service/ApiCall'

export default function ListScreen(props) {
  const { navigation, route } = props;
  const placeHolderSearch = 'Search a item'
  const [searchVal, onChangeSearchVal] = useState('')
  const [listVal, listEvent] = useState([])
  const [isLoading, loadEvent] = useState(false)
  const [page, pageEvent] = useState(1)
  const [loadMore, loadMoreEvent] = useState(false)

  useEffect(() => {

  })

  const renderItem = ({ item }) => {
    return (
      item != null ?
        <TouchableOpacity
          onPress={()=>{navigation.navigate('detail',{data: item})}}
          style={[containerStyle.tabListView]}
        >
          <Text>{item.id}</Text>
        </TouchableOpacity>
        : null
    )
  }

  const search = ()=>{
    requestCall(searchVal, page, (res)=>{
      loadEvent(false)
      listEvent(listVal=> listVal.concat(res.items))
      loadMoreEvent(res.incomplete_results)
    })
  }

  const onChangeText = (val)=>{
    onChangeSearchVal(val)
    if(val.length != 0 || searchVal == val.trim()){
      pageEvent(1)
      loadEvent(true)
      search()
    }else{
      pageEvent(1)
      listEvent([])
    }
  }

  const onEndReached = ()=>{
    if(!loadMore){
      pageEvent(page+1)
      search()
    }
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={[containerStyle.listView]}
    >
      <TextInput
        placeholder={placeHolderSearch}
        style={[textInputStyle.loginInputs]}
        onChangeText={(val) => onChangeText(val)}
        value={searchVal}
      />
      {isLoading ?
        <ActivityIndicator size="large" color='red'/>
        :
        <FlatList
          data={listVal}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={()=> onEndReached()}
          onEndReachedThreshold={0.5}
        />
      }
    </KeyboardAvoidingView>
  )
}


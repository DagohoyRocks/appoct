//request call function for the api
const URL = 'https://api.github.com/search/repositories'
export const requestCall = async (body, page=1, callBackFunc= null)=>{
  try{
    let responseJson = await fetch(URL+'?q='+body+'&page='+page).then(
      res=> res.json()
    );
    callBackFunc(responseJson)
  }
  catch(e){
    console.log('api error:', e)
  }
}
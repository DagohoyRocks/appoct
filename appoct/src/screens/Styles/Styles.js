import React from 'react';
import {StyleSheet} from 'react-native';

export const containerStyle = StyleSheet.create({
  loginView:{
    flex: 1,
    justifyContent: 'center',
    // backgroundColor: 'red'
  },
  listView:{
    flex: 1
  },
  tabListView:{
    alignItems: 'center',
    marginVertical: 5,
    paddingVertical: 20,
    paddingHorizontal: 5,
    borderRadius: 100,
    backgroundColor: '#F0F0F0',
    borderColor: '#707070'
  },
  detailView:{
    flex: 1,
    justifyContent: 'center',
  }
});

export const textInputStyle = StyleSheet.create({
  loginInputs:{
    marginVertical: 5,
    marginHorizontal: 5,
    paddingVertical: 10,
    paddingHorizontal: 5,
    borderRadius: 100,
    backgroundColor: '#F0F0F0',
    borderColor: '#707070'
  },
});

export const buttonStyle = StyleSheet.create({
  loginBtn:{
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
    marginHorizontal: 30,
    borderRadius: 100,
    backgroundColor: 'red'
  },
});

export const textStyle = StyleSheet.create({
  loginBtnTxt:{
    fontSize: 20,
  },
  label:{
    fontSize: 18,
    alignSelf: 'center'
  },
  input: {
    fontSize: 15,
    alignSelf: 'center'
  }
});